# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-18 02:18
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledge_base', '0007_auto_20170618_0216'),
    ]

    operations = [
        migrations.AddField(
            model_name='postmembership',
            name='date_created',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
