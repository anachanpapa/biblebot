# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-25 21:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('knowledge_base', '0034_auto_20170725_0423'),
    ]

    operations = [
        migrations.CreateModel(
            name='VerseEmotionMap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.FloatField(default=-1)),
            ],
            options={
                'ordering': ['emotion', 'score'],
            },
        ),
        migrations.CreateModel(
            name='VerseTopicMap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.FloatField(default=-1)),
            ],
            options={
                'ordering': ['plabel', 'score'],
            },
        ),
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['introduction']},
        ),
        migrations.AlterModelOptions(
            name='emotiontopicmap',
            options={'ordering': ['plabel', 'slabel']},
        ),
        migrations.AlterModelOptions(
            name='verse',
            options={'ordering': ['title']},
        ),
        migrations.AddField(
            model_name='versetopicmap',
            name='comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.Comment'),
        ),
        migrations.AddField(
            model_name='versetopicmap',
            name='plabel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.PostLabel'),
        ),
        migrations.AddField(
            model_name='versetopicmap',
            name='verse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.Verse'),
        ),
        migrations.AddField(
            model_name='verseemotionmap',
            name='comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.Comment'),
        ),
        migrations.AddField(
            model_name='verseemotionmap',
            name='emotion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.Emotion'),
        ),
        migrations.AddField(
            model_name='verseemotionmap',
            name='verse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='knowledge_base.Verse'),
        ),
    ]
