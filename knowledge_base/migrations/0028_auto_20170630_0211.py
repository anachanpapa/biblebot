# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-30 02:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledge_base', '0027_auto_20170626_0452'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='postmembership',
            options={'ordering': ['label']},
        ),
        migrations.RemoveField(
            model_name='postmembership',
            name='value',
        ),
        migrations.AddField(
            model_name='postmembership',
            name='score',
            field=models.FloatField(default=-1),
        ),
    ]
