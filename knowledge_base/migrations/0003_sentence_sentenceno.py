# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-16 00:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('knowledge_base', '0002_auto_20170616_0016'),
    ]

    operations = [
        migrations.AddField(
            model_name='sentence',
            name='sentenceNo',
            field=models.IntegerField(default=-1),
        ),
    ]
