from django.conf.urls import url
from django.template import loader
from . import views
from django.views.generic import TemplateView

app_name = 'knowledge_base'

urlpatterns = [
    url(r'^$', views.Home.as_view(), name='home'),
    url(r'^web/(?P<book_chapter>\w+)/(?P<focused_verses>[\d-]+)/$', views.WEB.as_view(), name='web'),
    url(r'^vent/', views.Vent, name='Vent'),
    # url(r'^load_classifiers/', views.load_classifiers, name='load_classifiers'),
    url(r'^overview$', views.Overview.as_view(), name='overview'),
    url(r'^sentence_labels$', views.SentenceLabels.as_view(), name='SentenceLabels'),
    url(r'^post_labels$', views.PostLabels.as_view(), name='PostLabels'),
    url(r'^anwser_rules$', views.AnswerRules.as_view(), name='AnswerRules'),
    url(r'^sentence_labeling/(?P<pk>[0-9]+)$', views.SentenceLabeling.as_view(), name='SentenceLabeling'),
    url(r'^sentence_registering/$', views.SentenceRegistering, name='SentenceRegistering'),
    url(r'^svm_learn_sentences/', views.svm_learn_sentences, name='svm_learn_sentences'),
    url(r'^svm_learn_posts/', views.svm_learn_posts, name='svm_learn_posts'),
    url(r'^initial_data/', views.initial_data, name='initial_data'),
    url(r'^initial_PostLabel/', views.initial_PostLabel, name='initial_PostLadata'),
    url(r'^initial_SentenceLabel/', views.initial_SentenceLabel, name='initial_SentenceLabel'),
    url(r'^initial_SentenceLabelSynonym/', views.initial_SentenceLabelSynonym, name='initial_SentenceLabelSynonym'),
    url(r'^initial_Sentence/', views.initial_Sentence, name='initial_Sentence'),
    url(r'^initial_Post/', views.initial_Post, name='initial_Post'),
    url(r'^initial_Verse/', views.initial_Verse, name='initial_Verse'),
    url(r'^initial_Emotion/', views.initial_Emotion, name='initial_Emotion'),
    url(r'^initial_EmotionTopicMap/', views.initial_EmotionTopicMap, name='initial_EmotionTopicMap'),
    url(r'^initial_Comment/', views.initial_Comment, name='initial_Comment'),
    url(r'^initial_VerseMap/', views.initial_VerseMap, name='initial_VerseMap'),
    url(r'^posts_into_catgories/', views.posts_into_catgories, name='posts_into_catgories'),
]