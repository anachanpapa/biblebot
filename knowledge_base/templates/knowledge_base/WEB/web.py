import re
import glob
import os.path

files = glob.glob('./orgHTMLs/*.htm')
for file in files:
    # print(file)
    org_file = file
    name = os.path.basename(org_file)
    new_file = './newHTMLs/' + name + 'l'
    # print(new_file)

    r = open(org_file, 'r')
    w = open(new_file, 'w')

    w.write("{% extends 'knowledge_base/base-web.html' %}")
    w.write("{% block content %}")
    verse_label = ""
    verse_id = ""
    # <span class="verse" id="V2">
    for line in r:
        if re.match(r"DOCTYPE", line) : continue
        if re.match(r"^(DOCTYPE|<html|</html|<head|</head|<meta|<line|<body|</body|<ul|</ul|<li|<p align|<hr />)", line) : continue
        if re.search(r'(class="x"|class="xt"|class="f"|class="ft")', line): continue
        line = line.replace('<a href="webfaq.htm">Frequently Asked Questions</a>', '')
        line = line.replace('<a href="index.htm">Downloads</a> <a href="http://eBible.org/cgi-bin/comment.cgi">Please report any typos you find.</a> ', '')
        line = line.replace('<a href="http://mljohnson.org/partners/">Donations</a>', '')
        line = line.replace("<a href='copyright.htm'>Public Domain</a>", '')
        line = line.replace('href="copyright.htm"', 'href="http://ebible.org/web/copyright.htm""')
        line = re.sub(r"<div class='chapterlabel' id=[^\>]+>[^\<]+</div>", "" , line)
        line = re.sub(r'<a href="#FN.+?<\/a>', '' , line)
        line = line.replace('href="copyright.htm"', '')
        line = line.replace('The World English Bible is in the Public Domain. You may copy and share it freely.', 'This website uses The World English Bible (Public Domain).')
        line = line.replace('<div class="copyright">', '<hr /><div class="copyright">')

        m = re.match(r'<title>World English Bible with Deuterocanon ([^<]+)</title>', line)
        if m: 
            verse_label = m.group(1)
            continue

        if re.match(r'^<div class="main">', line):
            line = line.replace('class="main"', 'class="chapter"')
            w.write(line)
            w.write('<div class="verse-label" >' + verse_label + '</div>\n')
            continue  

        # m = re.match(r'<span class="verse" id="([^\>]+)">', line)
        # if m: verse_id = m.group(1)

        if re.search(r'class="verse"', line):
            lines = line.split('>')
            for l in lines:

                if re.match('^\s*$', l): continue
                l = l + '>\n'
                if re.match("^<ul class='tnav'>", l): continue

                # m = re.match('^([^\<]+)(<span.*)$', l)
                # if m: 
                #     verse_body = m.group(1)
                #     tail = m.group(2)
                #     if not re.match(r'^\s*$', verse_body):
                #         l = '<span id="' + verse_id  + '_body">' + verse_body + '</span>' + tail

                # m = re.match('^([^\<]+)(</div.*)$', l)
                # if m: 
                #     verse_body = m.group(1)
                #     tail = m.group(2)
                #     l = '<span id="' + verse_id  + '_body">' + verse_body + '</span>' + tail



                m  = re.search(r'<span class=\"verse\" id=\"(.+)\"', l)
                if m: 
                    verse_id = m.group(1)
                    if verse_id == 'V1': 
                        l = re.sub('<span class=\"verse\"', '<span class="' + verse_id + '_NoBody"><span class=\"verse\"', l)
                        # l = '<span id="' + verse_id + '_NoBody">' + l
                    else:
                        l = re.sub('<span class=\"verse\"', '</span><span class="' + verse_id + '_NoBody"><span class=\"verse\"', l)
                        # l = '</span><span id="' + verse_id + '_NoBody">' + l


                if re.search(r"<div class='p", l) : 
                    l = re.sub(r"(<div class='p[^>]+?>)", r"\1<span class='" + verse_id + "_NoBody'>", l)

                elif re.search(r'<div class="p', l) : 
                    l = re.sub(r'(<div class="p[^>]+?>)', r"\1<span class='" + verse_id + "_NoBody'>", l)

                elif re.search(r"<div class='q", l) : 
                    l = re.sub(r"(<div class='q[^>]+?>)", r"\1<span class='" + verse_id + "_NoBody'>", l)

                elif re.search(r'<div class="q', l) : 
                    l = re.sub(r'(<div class="q[^>]+?>)', r"\1<span class='" + verse_id + "_NoBody'>", l)

                if re.search(r'</div>', l) : 
                    l = re.sub(r'</div>', r'</span></div>', l)


                if re.search('<div class="footnote">', l): 
                    l = '</span>' + l   



                w.write(l)
            continue
        w.write(line)



    w.write("{% endblock %}")
    r.close()
    w.close()