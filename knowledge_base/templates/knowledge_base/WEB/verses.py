import re

f = open('verses.txt', 'r')
for line in f:
    line = line.replace('\n', '')
    line = line.replace('\r', '')
    [verse, book] = line.split(',')
    book = book.replace('.html', '')
    m = re.match('^.+:(.+)$', verse)
    numbers = m.group(1)
    print('<a href="/knowledge_base/web/{0}/{2}/" target="_blank">{1}</a><br>'.format(book, verse, numbers))

f.close()