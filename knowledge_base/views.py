# coding=utf-8

import json
import re
import random
import copy
import time
import uuid
from collections import defaultdict 
import numpy as np
# import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from scw import SCW1
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.multiclass import OneVsRestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn. model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.externals import joblib
from django.http import Http404
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, DetailView
from knowledge_base.models import PostLabel, Post, PostMembership, SentenceLabel, SentenceLabelSynonym, Sentence, SentenceMembership, SentenceInPost, Verse, Emotion, EmotionTopicMap, Comment, VerseTopicMap, VerseEmotionMap, Question, Answer
from django.utils.functional import cached_property
from django.shortcuts import redirect
from django.db.models import Q
from django.core.cache import cache
from django.views.decorators.csrf import ensure_csrf_cookie

# class Labels(TemplateView):
# 	template_name='knowledge_base/labels.html'
# 	post_title = 'post labels'
# 	sentence_title = 'sentence labels'

# 	@cached_property
# 	def post_labels(self): return PostLabel.objects.all()	

# 	@cached_property
# 	def sentence_labels(self): return SentenceLabel.objects.all()


class Home(TemplateView):
	template_name='knowledge_base/home.html'
	title = 'BibleBot'

sentences_vectorizer = joblib.load('sentences.vectorizer')
sentences_classfier = joblib.load('svc-sentences.model')
posts_vectorizer= joblib.load('posts.vectorizer')
posts_classfier = joblib.load('svc-posts.model')
@ensure_csrf_cookie
def Vent(request):
	content = request.POST.get('item')
	content = re.split("\.|\n", content)
	# print(content)
	# cache.set('my_key', 'hello, world!', 30)
	# print(cache.get('my_key'))

	sentences = []
	for line in content:
		if re.match(r'^\s*$',line): continue 
		line = line.replace('\n',' ')
		line = line.replace('\r',' ')
		line = line.replace('\t',' ')
		line = line.replace('\\n',' ')
		line = line.replace('\\r',' ')
		line = line.replace('\\t',' ')
		line = line.replace('\\','')
		line = line.replace('^','')
		line = line.replace('\,','')
		line = line.replace('"title": null,','"title": "null",')
		line = line.replace('"body": null,','"body": "null",')
		line = line.replace('"','\\"')
		line = line.replace('\\"title\\": \\"','"title": "')
		line = line.replace('\\", \\"body\\": \\"','", "body": "')
		line = line.replace('\\", \\"url\\": \\"', '", "url": "')
		line = line.replace('\\"}', '"}')
		line = line.replace('\\"', '')
		line = re.sub(r'\s+',' ', line)
		line = re.sub(r'[\$\£\%\^\&\*\-\_\=\+\#\~\@]+','', line)
		line = re.sub(r'[0-9]+','', line)
		line = line.lower()
		line = line.strip()
		sentences.append(line)
		# print(line)

	try:
		post = ' '.join(sentences)
		emotion_adjustment = -0.6
		sentence_class_score = defaultdict(int)
		sentences = sentences_vectorizer.transform(sentences).toarray()
		pred_sentences_dist = sentences_classfier.decision_function(sentences)
		# print(pred_sentences_dist)
		for p in pred_sentences_dist:
			for cid in range(len(p)): 
				score = p[cid] 
				if score > emotion_adjustment: 
					sentence_class = sentences_classfier.classes_[cid]
					if sentence_class != 'NOT_EMOTION':
						sentence_class_score[sentence_class] += (score - emotion_adjustment)
		# print(sentence_class_score)

		post_class_score = defaultdict(int)
		post = posts_vectorizer.transform([post]).toarray()
		pred_posts_dist = posts_classfier.decision_function(post)
		pred_post_dist = pred_posts_dist[0]
		for cid in range(len(pred_post_dist)): 
			score = pred_post_dist[cid] 
			if score > 0: 
				post_class = posts_classfier.classes_[cid]
				post_class_score[post_class] = score
		# print(post_class_score)	

		# ventid = str(uuid.uuid4())
		reply_ordered = makeReply(post_class_score, sentence_class_score)
		# print(reply_ordered)
		return HttpResponse(json.dumps(reply_ordered), content_type='application/json')
	except:
		data = {'status': "notReady"}
		return HttpResponse(json.dumps(data), content_type='application/json')

@ensure_csrf_cookie
def load_classifiers(request):
	global sentences_vectorizer 
	global sentences_classfier 
	global posts_vectorizer 
	global posts_classfier 

	if sentences_vectorizer == None: sentences_vectorizer = joblib.load('sentences.vectorizer')
	if sentences_classfier == None: sentences_classfier = joblib.load('svc-sentences.model')
	if posts_vectorizer == None: posts_vectorizer= joblib.load('posts.vectorizer')
	if posts_classfier == None: posts_classfier = joblib.load('svc-posts.model')

	data = {'message': "classifiers loaded"}
	return HttpResponse(json.dumps(data), content_type='application/json')

def makeReply(post_class_score, sentence_class_score):
	# adjust score of post_class_score, sentence_class_score
	score = 16
	post_class_score_top5 = {}
	for k in reversed(sorted(post_class_score, key=lambda k:post_class_score[k])):
		post_class_score_top5[k] = score
		score = score/2
		if score < 1: break

	score = 16
	for k in reversed(sorted(sentence_class_score, key=lambda k:sentence_class_score[k])):
		sentence_class_score[k] = score
		score = score/2

	emotion_score = {}
	fractal = 0
	for p in post_class_score_top5:
		pInst = PostLabel.objects.get(title=p)
		for s in sentence_class_score:
			sInst = SentenceLabel.objects.get(title=s)
			sub = EmotionTopicMap.objects.filter(plabel=pInst, slabel=sInst).order_by('pk')
			for elem in sub:
				adjust = (1000 + fractal) / 1000
				score = post_class_score_top5[p] * sentence_class_score[s] * elem.score * adjust
				# score = post_class_score_top5[p] * sentence_class_score[s] * elem.score
				emotion_score[elem.emotion] = score
				fractal += 1
	
	# adjust score of emotion_score
	score = 16
	for k in reversed(sorted(emotion_score, key=lambda k:emotion_score[k])):
		emotion_score[k] = score
		score = score/2

	print(post_class_score_top5)
	print(emotion_score)

	reply_score = {}
	reply_content = {}
	fractal = 0
	for plabel in post_class_score_top5:
		pInst = PostLabel.objects.get(title=plabel)
		sub = VerseTopicMap.objects.filter(plabel=pInst).order_by('pk')
		for reply in sub:
			adjust = (1000 + fractal) / 1000
			key = (reply.verse.pk, reply.comment.pk)
			score = post_class_score_top5[plabel] * reply.score * adjust
			# score = post_class_score_top5[plabel] * reply.score
			if key in reply_score: score = max(score, reply_score[key])
			reply_score[key] = score
			verse = reply.verse
			comment = reply.comment
			content = {
				'verse_url': verse.url, 
				'verse_body': verse.body, 
				'introduction': comment.introduction,
				'postscript': comment.postscript
			}
			if not key in reply_content: reply_content[key] = content
			fractal += 1

	for emotion in emotion_score:
		emotionInst = Emotion.objects.get(title=emotion)
		sub = VerseEmotionMap.objects.filter(emotion=emotionInst).order_by('pk')
		for reply in sub:
			adjust = (1000 + fractal) / 1000
			key = (reply.verse.pk, reply.comment.pk)
			score = emotion_score[emotion] * reply.score * adjust
			# score = emotion_score[emotion] * reply.score
			if key in reply_score: score = max(score, reply_score[key])
			reply_score[key] = score
			verse = reply.verse
			comment = reply.comment
			content = {
				'verse_url': verse.url, 
				'verse_body': verse.body, 
				'introduction': comment.introduction,
				'postscript': comment.postscript
			}
			if not key in reply_content: reply_content[key] = content
			fractal += 1

	reply_ordered = {}
	rid = 0
	print('-- DEBUG --')
	for k in reversed(sorted(reply_score, key=lambda k:reply_score[k])):
		print(reply_score[k])
		print(reply_content[k])
		reply_ordered[rid] = reply_content[k]
		rid = rid + 1
		if rid >= 10: break

	# for k in sorted(reply_ordered.keys(), key=int): 
	# 	print(reply_ordered[k])
	return reply_ordered


class WEB(TemplateView):
	def get_context_data(self, **kwargs):
		context = super(WEB, self).get_context_data(**kwargs)
		context['focused_verses'] = self.kwargs['focused_verses']
		self.template_name ='knowledge_base/WEB/newHTMLs/' + self.kwargs['book_chapter'] + '.html'
		return context	

class Overview(TemplateView):
	template_name='knowledge_base/overview.html'
	title = 'Overview'


class SentenceLabels(TemplateView):
	template_name='knowledge_base/sentence_labels.html'
	title = 'Sentence Labels'

	slabels = SentenceLabel.objects.all()
	for slabel in slabels:
		slabel.positives = SentenceMembership.objects.filter(label=slabel).count()
		slabel.supervisors = SentenceMembership.objects.filter(label=slabel, supervise=False).count()
		slabel.save()

	@cached_property
	def sentence_labels(self): return SentenceLabel.objects.all().order_by('title')


class PostLabels(TemplateView):
	template_name='knowledge_base/post_labels.html'
	title = 'Post Labels'

	plabels = PostLabel.objects.all()
	for plabel in plabels:
		plabel.positives = PostMembership.objects.filter(label=plabel).count()
		plabel.save()

	@cached_property
	def post_labels(self): return PostLabel.objects.all().order_by('title')


class AnswerRules(TemplateView):
	template_name='knowledge_base/answer_rules.html'
	title = 'Anser Rules'


class SentenceLabeling(DetailView):
	template_name='knowledge_base/sentence_labeling.html'
	title = 'Sentence Labeling'
	model = SentenceLabel


	def get_context_data(self, **kwargs):
		context = super(SentenceLabeling, self).get_context_data(**kwargs)
		context['top10doc'] = self.select_sentences()
		context['other_labels'] = SentenceLabel.objects.all().exclude(Q(pk=self.kwargs['pk']) | Q(title='NOT_EMOTION') | Q(title='MULTI_EMOTIONS') | Q(title='UNKNOWN_EMOTION'))

		return context	

	def select_sentences(self):
		self.data_set()
		self.targetLabel  = self.kwargs['pk']

		selected = SentenceMembership.objects.filter(label=self.targetLabel, supervise=False).order_by('score').first()
		if selected == None: return self.top10doc_nosupervisor()
		supervisor = selected.sentence.sentenceNo
		supervisor = str(supervisor)
		return self.top10doc(supervisor)

	def data_set(self):
		self.vocabNum = 14150
		self.sentences = {}
		self.vectors = {}
		self.id2word = {}
		self.word2id = {}
		self.idfs = {}
		self.expands = {}
		data_path = 'knowledge_base/static/knowledge_base/CosineSimilarityData/'

		f = open(data_path + 'sentences.uniq.withID.withVector','r')
		for line in f:
			line = line.replace('\n','')
			line = line.replace('\r','')
			trio = line.split(';')
			id = trio[0]
			sentence = trio[1]
			vector = trio[2]
			self.sentences[id] = sentence
			self.vectors[id] = vector
		f.close()


		f = open(data_path + 'dic.expand.withID.withIDF','r')
		for line in f:
			line = line.replace('\n','')
			line = line.replace('\r','')
			tetra= line.split(';')
			id = tetra[0]
			word = tetra[1]
			self.id2word[id] = word
			self.word2id[word] = id

		f.close()	

		f = open(data_path + 'dic.expand.withID.withIDF','r')
		for line in f:
			line = line.replace('\n','')
			line = line.replace('\r','')
			tetra= line.split(';')
			id = tetra[0]
			word = tetra[1]
			idf = tetra[2]
			expand = tetra[3]
			self.idfs[id] = idf
			# print('--- ' + tetra[1] + ' ---')
			if not expand is 'null':
				try:
					# print('--- ' + word + ' --- ')	
					expand = json.loads(expand)
					top10expand = []
					for k in reversed(sorted(expand, key=lambda k:expand[k])):
						if k in self.word2id:
							top10expand.append(self.word2id[k]) 
							if len(top10expand) >= 10: break

					if len(top10expand) > 0: 
						self.expands[id] = top10expand						
				except:
					pass

		# print(len(self.expands))
		f.close()

	def top10doc(self, targetID):
		# target = self.vectors[targetID]
		print("--- TARGET ---")
		print(str(targetID) + ":" + self.sentences[targetID] + ":" + self.vectors[targetID] )
		# if re.match(r'^\s*$',self.sentences[targetID]):
		#	 return []

		""" set synonyms """
		synonyms = SentenceLabelSynonym.objects.filter(label=self.targetLabel)
		synonymlist = [self.word2id[synonym.synonym] for synonym in synonyms if synonym.synonym in self.word2id]

		wordNum = 0
		vectorStr = self.vectors[targetID]
		for e in vectorStr.split(','):
			[id, freq] = e.split(':')
			wordNum += int(freq)

		# synonyms vector:  weight of 3/4
		adjustRate = (wordNum / len(synonymlist)) * 3

		synonymlistStr = ''
		for synonym in synonymlist:
			pair = ',' + synonym + ':' + str(adjustRate)
			synonymlistStr += pair

		self.vectors[targetID] += synonymlistStr
		""" set synonyms end """

		print(self.vectors[targetID])

		scoreList = {}
		for docid in sorted(self.sentences.keys(), key=int):
			scoreList[docid] = self.score(self.vectors[targetID], self.vectors[docid])

		top10doc = [str(targetID) + ":" + self.sentences[targetID]] 
		i = 0
		j = 0 
		print("--- TOP10 ---")
		for k in reversed(sorted(scoreList, key=lambda k:scoreList[k])):
			if j % 20 != 0:
				j += 1
				continue
			
			if k == targetID: continue

			sInstance = Sentence.objects.get(sentenceNo=int(k))
			sm = SentenceMembership.objects.filter(sentence=sInstance)

			if sm.count() > 0 : 
				print('-- skip --')
				continue

			doc = k + ":" + self.sentences[k] + ":" + str(round(scoreList[k], 4))
			print(doc)
			top10doc.append(doc)

			i += 1
			if i >= 10: break
			j += 1
		return top10doc


	def top10doc_nosupervisor(self):
		# target = self.vectors[targetID]
		print("--- Without Supervisor ---")

		""" set synonyms """
		synonyms = SentenceLabelSynonym.objects.filter(label=self.targetLabel)
		synonymlist = [self.word2id[synonym.synonym] for synonym in synonyms if synonym.synonym in self.word2id]

		synonymlistStr = ''
		i = 0
		for synonym in synonymlist:
			if i == 0: pair = synonym + ':1' 
			else: pair = ',' + synonym + ':1' 
			synonymlistStr += pair
			i += 1

		""" set synonyms end """


		scoreList = {}
		for docid in sorted(self.sentences.keys(), key=int):
			scoreList[docid] = self.score(synonymlistStr, self.vectors[docid])

		top10doc = []
		i = 0
		j = 0 
		print("--- TOP10 ---")
		for k in reversed(sorted(scoreList, key=lambda k:scoreList[k])):
			if j % 20 != 0:
				j += 1
				# continue
			sInstance = Sentence.objects.get(sentenceNo=int(k))
			sm = SentenceMembership.objects.filter(sentence=sInstance)

			if sm.count() > 0 : 
				print('-- skip --')
				continue

			doc = k + ":" + self.sentences[k] + ":" + str(round(scoreList[k], 4))
			print(doc)
			top10doc.append(doc)
			i += 1
			if i >= 10: break
			j += 1
		return top10doc



	def score(self, target, doc):
		t = np.zeros(self.vocabNum)
		d = np.zeros(self.vocabNum)

		if re.match(r'^\s*$',doc): return 0
		for p in target.split(','):
			pair = p.split(':')
			id = pair[0]
			idnum= int(pair[0])
			freq = float(pair[1])
			idf = float(self.idfs[pair[0]])
			idf = round(idf,1)
			weight = freq * idf
			t[idnum] += weight

			# expand word with word2vec
			# if id in self.expands: 
			# 	for e in self.expands[id]: t[int(e)] = weight

		for p in doc.split(','):
			pair = p.split(':')
			id = int(pair[0])
			freq = float(pair[1])
			idf = float(self.idfs[pair[0]])
			idf = round(idf,1)
			d[id] = freq * idf

		ip = np.dot(t, d)
		score = ip/(np.linalg.norm(t) * np.linalg.norm(d))
		return score


def SentenceRegistering(request):
	label_pk = int(request.POST.get('label-pk'))
	labelInstance = SentenceLabel.objects.get(pk=label_pk)
	if request.POST.get('supervisor') != None:
		supervisor = int(request.POST.get('supervisor'))
		sInstance = Sentence.objects.get(sentenceNo=supervisor)
		sv = SentenceMembership.objects.get(label=labelInstance, sentence=sInstance)
		sv.supervise = True
		sv.save()
	

	a = 0
	if request.POST.get('supervisor') != None: 	a = 1

	for i in range(a, a+10):
		# try:
		sid = int(request.POST.get('sid-' + str(i)))
		sInstance =Sentence.objects.get(sentenceNo=sid)
		score = request.POST.get('score-' + str(i))
		value = request.POST.get('label-' + str(i))

		if value == "NOE":
			label = SentenceLabel.objects.get(title='NOT_EMOTION')			
			sm = SentenceMembership(
				label=label, 
				sentence=sInstance, 
				score=score
				)

		elif value == "MUE":
			label = SentenceLabel.objects.get(title='MULTI_EMOTIONS')	
			sm = SentenceMembership(
				label=label, 
				sentence=sInstance, 
				score=score
				)			

		elif value == "UNK":
			label = SentenceLabel.objects.get(title='UNKNOWN_EMOTION')	
			sm = SentenceMembership(
				label=label, 
				sentence=sInstance, 
				score=score
				)	

		elif value == "POS":
			sm = SentenceMembership(
				label=labelInstance, 
				sentence=sInstance, 
				score=score
				)					
		else:
			label = SentenceLabel.objects.get(pk=int(value))	
			sm = SentenceMembership(
				label=label, 
				sentence=sInstance, 
				score=score
				)

		sm.save()
		# except:
		# 	pass

	slabels = SentenceLabel.objects.all()
	for slabel in slabels:
		slabel.positives = SentenceMembership.objects.filter(label=slabel).count()
		slabel.supervisors = SentenceMembership.objects.filter(label=slabel, supervise=False).count()
		slabel.save()

	return redirect('knowledge_base:SentenceLabeling', pk=label_pk)



# SVM Learn
def svm_learn_sentences(request):
	# make data

	# Emotion Classes
	notem = SentenceLabel.objects.get(title='NOT_EMOTION')
	multi = SentenceLabel.objects.get(title='MULTI_EMOTIONS')
	unknown = SentenceLabel.objects.get(title='UNKNOWN_EMOTION')
	# sms = SentenceMembership.objects.all().exclude(Q(label=notem) | Q(label=multi) | Q(label=unknown))
	sms = SentenceMembership.objects.all().exclude(Q(label=multi) | Q(label=unknown))
	train_X = [sm.sentence.body for sm in sms]
	train_Y = [sm.label.title for sm in sms]
	svm_learn_sentences_processing(train_X, train_Y)
	# classifier_comparison(train_X, train_Y)

	# Not Emotions
	# multi = SentenceLabel.objects.get(title='MULTI_EMOTIONS')
	# unknown = SentenceLabel.objects.get(title='UNKNOWN_EMOTION')
	# sms = SentenceMembership.objects.all().exclude(Q(label=multi) | Q(label=unknown))
	# train_X = [sm.sentence.body for sm in sms]
	# train_Y = [sm.label.title if sm.label.title == 'NOT_EMOTION' else 'EMOTIONS' for sm in sms]
	# print('--- train_y ---')
	# print(train_y)
	# svm_learn_sentences_processing(train_X, train_Y)

	return HttpResponse(json.dumps('svm_learn_sentences ended'), content_type='application/json')


def svm_learn_sentences_processing(train_X, train_Y):

	# look at sequences of tokens of minimum length 1 and maximum length 3
	vectorizer = CountVectorizer(ngram_range=(1, 2))
	# vectorizer = TfidfVectorizer(ngram_range=(1, 1))
	vectorizer.fit(train_X)
	joblib.dump(vectorizer, 'sentences.vectorizer', compress=True)
	vectorizer = joblib.load('sentences.vectorizer')
	train_x_org = vectorizer.transform(train_X).toarray()
	# train_x_org = preprocessing.normalize(train_x_org, norm='l2')
	# vocabulary = vectorizer.get_feature_names()
	train_y_org = np.array(train_Y)
	print(train_x_org.shape)


	print('----- train_x (' + str(len(train_x_org)) + ' ) -----')
	# print(type(train_x_org))
	print('----- train_y (' + str(len(train_y_org)) + ' ) -----')
	# print(type(train_y_org))
	# print(vocabulary)
	# print('----- train_y (' + str(len(train_y)) + ' ) -----')
	# print(train_y)

	train_x, train_y, test_x, test_y, test_sentences = [], [], [], [], []
	idxs = [i for i in range(len(train_X))]
	random.shuffle(idxs)
	test_size = len(idxs) * 0.25
	for idx in idxs:
		if len(test_x) <= test_size:
			test_x.append(train_x_org[idx])
			test_y.append(train_y_org[idx])
			test_sentences.append(train_X[idx])
		else:
			train_x.append(train_x_org[idx])
			train_y.append(train_y_org[idx])

	# train_x, test_x, train_y, test_y  = train_test_split(train_x_org, train_y)

	C = 1000
	kernel = 'rbf'
	gamma  = 0.0001
	degree = 2
	# estimator = svm.SVC(C=C, kernel=kernel, gamma=gamma)
	# estimator = svm.SVC(C=C, kernel='poly', degree=degree)
	estimator = svm.LinearSVC(C=1000)
	multi_svm = OneVsRestClassifier(estimator)
	multi_svm.fit(train_x, train_y)
	joblib.dump(multi_svm, 'svc-sentences.model', compress=True)
	multi_svm = joblib.load('svc-sentences.model')
	pred_y = multi_svm.predict(test_x)

	pred_y_rare = copy.deepcopy(pred_y)
	pred_y_dist = multi_svm.decision_function(test_x)
	# print('----- decision_function -----')
	# print(len(pred_y_dist))
	print('----- class list -----')
	print(multi_svm.classes_)
	# print(len(pred_y_dist[0]))
	# print(pred_y_dist[0])
	# print(pred_y[0])

	print(len(test_y))
	# print(len(pred_y))
	print('One-against-rest: {:.5f}'.format(accuracy_score(test_y, pred_y)))


	
	for idx in range(len(pred_y_dist)):
		if pred_y_dist[idx][0] > 0: 
			continue
		else:
			pred_y_dist[idx][0] = -100
			strongest_emotion_id = np.argmax(pred_y_dist[idx])
			strongest_emotion = multi_svm.classes_[int(strongest_emotion_id)]
			print(strongest_emotion)
			pred_y[idx] = strongest_emotion 

	# evaluateion of trained model
	count = {}
	emergence = defaultdict(int)
	errors = defaultdict(int)
	for i in range(len(test_y)):
		ty = test_y[i]
		py = pred_y[i]
		if ty in count: 
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
			else: 
				if py == 'NOT_EMOTION': 
					print(test_sentences[i]) 
					print(pred_y_dist[i])
				errors[(ty, py)] += 1
						
		else:
			count[ty] = {'all':0 , 'correct':0}
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
			else: 
				if py == 'NOT_EMOTION': 
					print(test_sentences[i])
					print(pred_y_dist[i])
				errors[(ty, py)] += 1					
		emergence[py] += 1	

	for k in sorted(count.keys()):
		if k == 'EMOTIONS': continue
		print(k , end="-->")
		labelInstance = SentenceLabel.objects.get(title=k)
		labelInstance.recall = round(count[k]['correct']/count[k]['all'], 2)
		print('recall: {0}({1}/{2})'.format(round(count[k]['correct']/count[k]['all'], 2), count[k]['correct'], count[k]['all']), end=' ')
		if emergence[k] > 0:
			labelInstance.precision = round(count[k]['correct']/emergence[k], 2)
			print('precision: {0}({1}/{2})'.format(round(count[k]['correct']/emergence[k], 2), count[k]['correct'], emergence[k]))
		else:
			labelInstance.precision = 0

		labelInstance.save()
		

	print('')
	print('--- ERRPR REPORT ---')
	for k in sorted(errors.keys()):
		print("{0} misclassified as {1}: {2}".format(k[0], k[1], errors[k]))


	print('')
	print('--- RARE RESULT ---')
	count = {}
	emergence = defaultdict(int)
	for i in range(len(test_y)):
		ty = test_y[i]
		py = pred_y_rare[i]
		if ty in count: 
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
		else:
			count[ty] = {'all':0 , 'correct':0}
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
		emergence[py] += 1	

	for k in sorted(count.keys()):
		if k == 'EMOTIONS': continue
		print(k , end=' --> ')
		print('recall: {0}({1}/{2})'.format(round(count[k]['correct']/count[k]['all'], 2), count[k]['correct'], count[k]['all']), end=' ')
		if emergence[k] > 0:
			print('precision: {0}({1}/{2})'.format(round(count[k]['correct']/emergence[k], 2), count[k]['correct'], emergence[k]))

	# SVM Learn
	# print("[INFO] SVM (Grid Search)")
	# svm_tuned_parameters = [
	#	 {
	#		 'kernel': ['rbf'],
	#		 'gamma': [2**n for n in range(-15, 3)],
	#		 'C': [2**n for n in range(-5, 15)]
	#	 }
	# ]
	# gscv = GridSearchCV(
	#	 svm.SVC(),
	#	 svm_tuned_parameters,
	#	 cv=5,	  # クロスバリデーションの分割数
	#	 n_jobs=1,  # 並列スレッド数
	#	 verbose=3  # 途中結果の出力レベル 0 だと出力しない
	# )
	# gscv.fit(train_x, train_y)
	# svm_model = gscv.best_estimator_  # 最も精度の良かったモデル
	# print(svm_model)

	# grid search



	# cross validation
	


	# return data set

def svm_learn_posts(request):
	pms = PostMembership.objects.all()
	train_X = []
	for i in range(len(pms)):
		body = pms[i].post.body
		title = pms[i].post.title
		both = title + ' ' + body
		train_X.append(both)

	# train_X = [pm.post.body for pm in pms]
	train_Y = [pm.label.title for pm in pms]
	# svm_learn_posts_processing(train_X, train_Y)
	classifier_comparison(train_X, train_Y)

	return HttpResponse(json.dumps('svm_learn_sentences ended'), content_type='application/json')


def svm_learn_posts_processing(train_X, train_Y):

	# look at sequences of tokens of minimum length 1 and maximum length 3
	vectorizer = CountVectorizer(ngram_range=(1, 2))
	# vectorizer = TfidfVectorizer(ngram_range=(1, 1))
	vectorizer.fit(train_X)
	joblib.dump(vectorizer, 'posts.vectorizer', compress=True)
	vectorizer = joblib.load('posts.vectorizer')	
	train_x_org = vectorizer.transform(train_X)
	train_x_org = preprocessing.normalize(train_x_org, norm='l2')
	train_y_org = np.array(train_Y)
	print(train_x_org.shape)

	# train_x, train_y, test_x, test_y, test_sentences = [], [], [], [], []
	# idxs = [i for i in range(len(train_y_org))]
	# random.shuffle(idxs)
	# test_size = len(idxs) * 0.25
	# for idx in idxs:
	# 	if len(test_x) <= test_size:
	# 		test_x.append(train_x_org[idx])
	# 		test_y.append(train_y_org[idx])
	# 		test_sentences.append(train_X[idx])
	# 	else:
	# 		train_x.append(train_x_org[idx])
	# 		train_y.append(train_y_org[idx])

	indices = np.arange(train_x_org.shape[0])
	train_x, test_x, train_y, test_y, train_idx, test_idx = train_test_split(train_x_org, train_y_org, indices, test_size=0.25)
	estimator = svm.LinearSVC(C=1000)
	multi_svm = OneVsRestClassifier(estimator)
	multi_svm.fit(train_x, train_y)
	joblib.dump(multi_svm, 'svc-posts.model', compress=True)
	multi_svm = joblib.load('svc-posts.model')
	pred_y = multi_svm.predict(test_x)
	# pred_y_dist = multi_svm.decision_function(test_x)

	print('train_idx:', str(len(train_idx)))
	print('test_idx:', str(len(test_idx)))

	print(len(test_y))
	print('One-against-rest: {:.5f}'.format(accuracy_score(test_y, pred_y)))

	
	# evaluateion of trained model
	count = {}
	emergence = defaultdict(int)
	errors= defaultdict(int)
	# f = open('error_posts.txt' , 'a')
	for i in range(len(test_y)):
		ty = test_y[i]
		py = pred_y[i]
		if ty in count: 
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
			else: 
				errors[(ty, py)] += 1
				# f.write("{0} misclassified as {1} -----> {2}\n".format(ty, py, train_X[test_idx[i]]))
		else:
			count[ty] = {'all':0 , 'correct':0}
			t_p = count[ty]
			t_p['all'] += 1
			if ty == py: t_p['correct'] += 1
			else: 
				errors[(ty, py)] += 1	
				# f.write("{0} misclassified as {1} -----> {2}\n".format(ty, py, train_X[test_idx[i]]))
		emergence[py] += 1	

	# f.close()

	for k in sorted(count.keys()):
		print(k , end=' --> ')
		labelInstance = PostLabel.objects.get(title=k)
		labelInstance.recall = round(count[k]['correct']/count[k]['all'], 2)
		print('recall: {0}({1}/{2})'.format(round(count[k]['correct']/count[k]['all'], 2), count[k]['correct'], count[k]['all']), end=' ')
		if emergence[k] > 0:
			labelInstance.precision = round(count[k]['correct']/emergence[k], 2)
			print('precision: {0}({1}/{2})'.format(round(count[k]['correct']/emergence[k], 2), count[k]['correct'], emergence[k]))
		else:
			labelInstance.precision = 0

		labelInstance.save()
		

	print('')
	print('--- ERRPR REPORT ---')
	for k in sorted(errors.keys()):
		print("{0} misclassified as {1}: {2}".format(k[0], k[1], errors[k]))


def classifier_comparison(train_X, train_Y):
	vectorizer = CountVectorizer(ngram_range=(1, 2))
	# vectorizer = TfidfVectorizer(ngram_range=(1, 2))
	dicsize = len(train_X[0])
	vectorizer.fit(train_X)
	train_X = vectorizer.transform(train_X)
	train_X = preprocessing.normalize(train_X, norm='l2')

	# pca = PCA(n_components=500)
	# pca.fit(train_X)
	# train_X = pca.transform(train_X)

	print(train_X.shape)
	# train_X = vectorizer.transform(train_X).toarray()
	# vocabulary = vectorizer.get_feature_names()
	train_Y= np.array(train_Y)
	train_x, test_x, train_y, test_y  = train_test_split(train_X, train_Y, test_size=0.3)

	names = ["Nearest Neighbors", "Linear SVM", "RBF SVM", "Decision Tree",
		 "Random Forest", "AdaBoost"]

	classifiers = [
		KNeighborsClassifier(3),
		# SCW1(C=1.0, ETA=1.0),
		svm.SVC(kernel="linear", C=1000),
		svm.SVC(kernel="rbf", gamma=0.0001, C=1000),
		DecisionTreeClassifier(max_depth=7),
		RandomForestClassifier(max_depth=7, n_estimators=10, max_features=1),
		AdaBoostClassifier()]

	print('')
	print('--- Classfier Comparison ---')
	for name, clf in zip(names, classifiers):
		clf.fit(train_x, train_y)
		score = clf.score(test_x, test_y)
		print("name: {0} -- score: {1}".format(name, score))


# INITIALIZE DATA

def initial_data(request):
	context = {}
	# return HttpResponse(template.render(context, request))
	return render(request, 'knowledge_base/init.html', context)



def initial_PostLabel(request):
	f = open('knowledge_base/static/knowledge_base/initial_data/PostLabel.txt','r') 

	items = PostLabel.objects.all()
	for item in items: item.delete()

	for label in f:
		label = label.replace('\n','') 
		label = label.replace(' ','') 
		q = PostLabel(title=label)
		q.save()

	f.close()
	data = {'message': "%s items added" % len(PostLabel.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')
	# if request.is_ajax() and request.POST:
	# 	data = {'message': "%s added" % request.POST.get('item')}
	# 	return HttpResponse(json.dumps(data), content_type='application/json')
	# else:
	# 	raise Http404



def initial_SentenceLabel(request):
	f = open('knowledge_base/static/knowledge_base/initial_data/SentenceLabel.txt','r') 

	items = SentenceLabel.objects.all()
	for item in items: item.delete()

	for label in f:
		label = label.replace('\n','') 
		label = label.replace(' ','') 
		q = SentenceLabel(title=label)
		q.save()

	f.close()
	data = {'message': "%s items added" % len(SentenceLabel.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_SentenceLabelSynonym(request):
	# print('kuru')
	f = open('knowledge_base/static/knowledge_base/initial_data/SentenceLabelSynonym.txt','r') 

	items = SentenceLabelSynonym.objects.all()
	for item in items: item.delete()

	for pair in f:
		pair = pair.replace('\n','') 
		[label, synonym] = pair.split(',')
		labelInstance = SentenceLabel.objects.get(title=label)
		q = SentenceLabelSynonym(label=labelInstance, synonym=synonym)
		q.save()
	f.close()
	data = {'message': "%s items added" % len(SentenceLabelSynonym.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_Sentence(request):
	print('initial_Sentence')
	f = open('knowledge_base/static/knowledge_base/initial_data/sentences.uniq.withID','r') 

	items = Sentence.objects.all()
	for item in items: item.delete()
	print('all sentenses deleted')

	i = 0
	for line in f:
		line = line.replace('\n','') 
		line = line.strip()
		duo = line.split(';')
		sentenceNo = duo[0]
		body = duo[1]
		q = Sentence(sentenceNo=sentenceNo, body=body)
		q.save()
		i += 1
		if i%1000 == 0: print('-- ' + str(i) + ' -- Done')

	f.close()
	data = {'message': "%s items added" % len(Sentence.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_Post(request):
	print('initial_Post')
	items = Post.objects.all()
	for item in items: item.delete()
	print('all posts deleted')

	f = open('knowledge_base/static/knowledge_base/initial_data/therapy-clean.jl','r') 
	initial_Post_proceed(f)
	f.close()

	f = open('knowledge_base/static/knowledge_base/initial_data/psychforums.jl','r') 
	initial_Post_proceed(f)
	f.close()

	data = {'message': "%s items added" % len(Post.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')
	# 	title = re.sub(r'[\:\,\{\}]+','', title)
	# 	m = re.search(r"^[\'\"]+(.+)[\'\"]+$", title)
	# 	if m: title = m.group(1)
	# 	if not re.match(r"^\s*$", title) and len(title) > 1 :
	# 		title.strip()
	# 		title = sentence_clean(title)
	# 		if title != None:
	# 			row = sentence2pk(title)
	# 			if row != None:
	# 				q = SentenceInPost(sentence=row, post=post)
	# 				q.save()
		
	# 	body = re.sub(r'[\:\,\{\}]+','', body)
	# 	m = re.search(r"^[\'\"]+(.+)[\'\"]+$", body)
	# 	if m: body = m.group(1)
	# 	sentences = re.split("[\.\!\?\:\;\(\)\[\]\<\>\/]", body)
	# 	for sentence in sentences:
	# 		sentence.strip()
	# 		if re.match(r"^\s*$", sentence): continue
	# 		if len(sentence) <= 3: continue
	# 		sentence = sentence_clean(sentence)
	# 		if sentence != None:
	# 			row = sentence2pk(sentence)
	# 			if row != None:
	# 				q = SentenceInPost(sentence=row, post=post)
	# 				q.save()

def initial_Post_proceed(f):
	for line in f:
		line = line.replace('\n',' ')
		line = line.replace('\r',' ')
		line = line.replace('\t',' ')
		line = line.replace('\\n',' ')
		line = line.replace('\\r',' ')
		line = line.replace('\\t',' ')
		line = line.replace('\\','')
		line = line.replace('^','')
		line = line.replace('\,','')
		line = line.replace('"title": null,','"title": "null",')
		line = line.replace('"body": null,','"body": "null",')
		line = line.replace('"','\\"')
		line = line.replace('\\"title\\": \\"','"title": "')
		line = line.replace('\\", \\"body\\": \\"','", "body": "')
		line = line.replace('\\", \\"url\\": \\"', '", "url": "')
		line = line.replace('\\"}', '"}')
		line = line.replace('\\"', '')
		line = re.sub(r'\s+',' ', line)
		line = re.sub(r'[\$\£\%\^\&\*\_\=\+\#\~\@]+','', line)
		line = re.sub(r'[0-9]+','', line)
		line = line.lower()

		l = json.loads(line)
		title = l['title']
		body =l['body']
		body = re.sub(r'[\$\£\%\^\&\*\-\_\=\+\#\~\@]+','', body)
		url =l['url']

		post = Post(title=title, body=body, url=url)
		post.save()

		m = re.match(r"http://blahtherapy.com/user-groups/([^\/]+)/forum/", url)
		if m == None: m =re.match(r"http://www.psychforums.com/([^\/]+)/topic", url)
		forum = m.group(1)
		forum_map = {
		'addictions' : 'addiction',
		'addictionsaddicts' : 'addiction',
		'age-gap-relationships' : 'relationship',
		'body-posi' : 'appearance',
		'christianity' : 'god',
		'cut-cut-cut' : 'self_injury',
		'dating-relationship-gurus' : 'relationship',
		'death-and-grieving' : 'death',
		'divorce' : 'relationship',
		'friendships' : 'relationship',
		'lesbian-talk-help-and-advice' : 'mental_illness_complex_origin_sin',
		'lgbt-support' : 'mental_illness_complex_origin_sin',
		'long-distance' : 'relationship',
		'loverelationships' : 'relationship',
		'misunderstood' : 'relationship',
		'pickersself-harm' : 'self_injury',
		'ptsd-' : 'mental_illness_trauma_origin',
		'recovering-from-abuse' : 'mental_illness_trauma_origin',
		'scars' : 'self_injury',
		'self-injury-support-group' : 'self_injury',
		'sexual-assault-survivors' : 'mental_illness_trauma_origin',
		'sleep-and-sleep-disorders' : 'sleep_disorder',
		'sleepless-nights' : 'sleep_disorder',
		'social-anxiety' : 'social_anxiety',
		'social-anxiety-support' : 'social_anxiety',
		'socially-awkward' : 'social_anxiety',
		'strange-andor-deviant-sexual-addictions' : 'addiction_sin',
		# psychforums
		'agoraphobia' : 'mental_illness_fear_origin',
		'alzheimer' : 'disability',
		'anorexia-nervosa' : 'anorexia',
		'asperger-syndrome' : 'disability',
		'attention-deficit-hyperactivity' : 'disability',
		'autism' : 'disability',
		'binge-eating' : 'mental_illness_stress_origin',
		'body-dysmorphic-disorder' : 'appearance',
		'bulimia-nervosa' : 'anorexia',
		'bullying' : 'abuse',
		'cognitive-disorder' : 'disability',
		'compulsive-lying' : 'mental_illness_complex_origin_sin',
		'cutting-self-injury' : 'self_injury',
		'delirium' : 'disability',
		'dementia' : 'disability',
		'depersonalization' : 'mental_illness_trauma_origin',
		'dissociative-amnesia' : 'mental_illness_trauma_origin',
		'dissociative-disorder' : 'mental_illness_trauma_origin',
		'dissociative-fugue' : 'mental_illness_trauma_origin',
		'dissociative-identity' : 'mental_illness_trauma_origin',
		'domestic-abuse' : 'abuse',
		'food-addiction' : 'addiction',
		'gambling-addiction' : 'gambling',
		'gender-identity-disorder' : 'mental_illness_complex_origin_sin',
		'generalized-anxiety' : 'mental_illness_fear_origin',
		'grief-loss' : 'death',
		'insomnia' : 'sleep_disorder',
		'intermittent-explosive-disorder' : 'mental_illness_unknown_origin',
		'internet-addiction' : 'addiction',
		'marriage-divorce' : 'relationship',
		'nightmare-disorder' : 'nightmare',
		'obsessive-compulsive' : 'mental_illness_fear_origin',
		'parenting-forum' : 'relationship',
		'post-traumatic-stress' : 'mental_illness_trauma_origin',
		'primary-sleep' : 'sleep_disorder',
		'rape-sexual-assault' : 'abuse',
		'reactive-attachment' : 'mental_illness_dissatisfaction_origin',
		'selective-mutism' : 'mental_illness_fear_origin',
		'sexual-addiction' : 'addiction_sin',
		'shoplifting-addiction' : 'addiction_sin',
		'social-phobia' : 'social_anxiety',
		'stuttering' : 'disability',
		'substance-abuse' : 'addiction',
		'tourette-syndrome' : 'mental_illness_unknown_origin',
		'trichotillomania' : 'mental_illness_unknown_origin',
		}
		
		if forum in forum_map:
			label = PostLabel.objects.get(title=forum_map[forum]) 
			post_membership = PostMembership(label=label, post=post)
			post_membership.save()


def posts_into_catgories(request):
	# data = {'message': "%s items added" % len(Sentence.objects.all())}
	posts = Post.objects.all()
	f = open('posts_into_categories.txt','w') 
	for post in posts:
		if PostMembership.objects.filter(post=post).count() > 0:
			continue
		else:
			body = post.body
			if re.search(r"cheated", body): 
				f.write(body)
				f.write('\n')

	data = {'message': "items added" }
	return HttpResponse(json.dumps(data), content_type='application/json')
	f.close()



def initial_Verse(request):
	print('initial_Verse')
	f = open('knowledge_base/static/knowledge_base/initial_data/verse-file-url.txt','r') 

	items = Verse.objects.all()
	for item in items: item.delete()
	print('all Verses deleted')

	for line in f:
		line = line.replace('\n','') 
		line = line.strip()
		[title, file, body] = line.split('$$$')
		file = file.replace('.htm', '')
		m = re.match('^.+:(.+)$', title)
		numbers = m.group(1)		
		url = '<a href="/knowledge_base/web/' + file + '/' + numbers + '/" target="_blank">' + title + '</a>'
		q = Verse(title=title, url=url, body=body)
		q.save()

	f.close()
	data = {'message': "%s items added" % len(Verse.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_Emotion(request):
	print('initial_Emotion')
	f = open('knowledge_base/static/knowledge_base/initial_data/emotions.txt','r') 

	items = Emotion.objects.all()
	for item in items: item.delete()
	print('all Emotions deleted')

	for line in f:
		line = line.replace('\n','') 
		title = line.strip()
		q = Emotion(title=title)
		q.save()

	f.close()
	data = {'message': "%s items added" % len(Emotion.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_Comment(request):
	print('initial_Comment')
	f = open('knowledge_base/static/knowledge_base/initial_data/comment.txt','r') 

	items = Comment.objects.all()
	for item in items: item.delete()
	print('all Comments deleted')

	for line in f:
		line = line.replace('\n','') 
		line = line.strip()
		array = line.split('[comment]')
		if len(array) == 2:
			introduction = array[0]
			postscript = array[1]
			q = Comment(introduction=introduction, postscript=postscript)
		else:
			introduction = line 
			q = Comment(introduction=introduction)
		q.save()

	f.close()
	data = {'message': "%s items added" % len(Comment.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')

def initial_EmotionTopicMap(request):
	print('initial_Emotion')

	items = EmotionTopicMap.objects.all()
	for item in items: item.delete()
	print('all EmotionTopicMap deleted')

	row = ['abuse','addiction','addiction_sin','appearance','death','disability','anorexia','gambling','god','mental_illness_fear_origin','mental_illness_dissatisfaction_origin','mental_illness_trauma_origin','mental_illness_unknown_origin','mental_illness_stress_origin','mental_illness_complex_origin_sin','nightmare','relationship','self_injury','sleep_disorder','social_anxiety']
	column = ['anger(to people long term)','anger(to people short temper)','anger(to unjustice)','anger or frustration (to situation)','depressed (no hope in people)','depressed (no hope in situation)','depressed (no hope in myself)','depressed (no hope in future)','depressed (struggling with grief)','fear (to people)','fear (to situation)','guilt (to God)','guilt (might be just feeling guilty)','jealousy','lack of love (patience)','lack of love (kindness)','lack of motivation','lack of self_control','loneliness','shame (infront of people)','shame (infront of yourself)','want happiness','want love','want respect','want salvation']
	topic2emotion = [
		[8,0,8,4,4,4,4,4,2,8,4,0,2,0,0,0,0,2,4,2,2,4,8,2,4],
		[0,0,0,2,0,8,8,8,0,0,8,4,0,0,0,0,4,8,0,4,8,0,0,0,4],
		[0,0,0,2,0,8,8,8,0,0,8,8,0,0,0,0,4,8,0,4,8,0,0,0,4],
		[0,0,0,2,0,8,8,8,4,4,4,0,0,8,0,0,0,0,0,8,8,8,8,2,4],
		[0,0,0,0,0,8,0,8,8,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0],
		[0,2,2,0,4,8,8,8,4,2,0,0,2,0,0,0,0,0,8,4,4,8,8,2,4],
		[0,0,0,0,0,0,0,0,0,2,4,0,0,4,0,0,0,0,0,4,8,8,8,2,4],
		[0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0,4,8,0,4,8,0,0,0,4],
		[0,0,0,0,0,4,4,4,4,0,0,8,0,0,0,4,4,0,0,0,4,4,4,0,8],
		[2,0,4,4,0,0,0,0,0,8,8,0,0,0,0,0,0,0,0,4,4,0,0,0,8],
		[0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,8,16,0,8],
		[8,0,4,4,8,4,4,4,8,8,4,0,0,0,0,0,0,0,0,4,4,4,8,2,8],
		[4,0,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,4,4,4,4,4,4,4],
		[4,0,4,4,0,0,0,0,0,4,4,0,0,4,0,0,0,0,0,4,4,0,0,0,4],
		[0,0,0,4,4,8,8,8,8,4,4,2,0,8,0,0,0,4,8,8,8,4,8,2,8],
		[4,0,2,2,2,2,4,4,4,8,8,4,2,2,0,0,0,0,0,2,2,0,0,0,8],
		[8,4,4,4,4,4,4,4,4,4,4,2,2,4,4,4,0,4,4,4,4,4,8,4,0],
		[4,0,4,0,4,4,4,4,4,4,4,8,2,0,0,0,0,0,4,4,4,8,8,0,8],
		[4,0,4,4,4,4,4,4,4,4,4,4,2,4,0,0,0,0,0,4,4,0,0,0,2],
		[0,0,0,4,0,0,0,0,0,8,4,0,0,4,0,0,0,4,0,4,4,0,0,2,0]
	]

	emotion2slabel = {
		'anger(to people long term)' : 'anger',
		'anger(to people short temper)' : 'anger',
		'anger(to unjustice)' : 'anger',
		'anger or frustration (to situation)' : 'anger',
		'depressed (no hope in people)' : 'depressed',
		'depressed (no hope in situation)' : 'depressed',
		'depressed (no hope in myself)' : 'depressed',
		'depressed (no hope in future)' : 'depressed',
		'depressed (struggling with grief)' : 'depressed',
		'fear (to people)' : 'fear',
		'fear (to situation)' : 'fear',
		'guilt (to God)' : 'guilt',
		'guilt (might be just feeling guilty)' : 'guilt',
		'jealousy' : 'jealousy',
		'lack of love (patience)' : 'lack of love',
		'lack of love (kindness)' : 'lack of love',
		'lack of motivation' : 'lack of motivation',
		'lack of self_control' : 'lack of self_control',
		'loneliness' : 'loneliness',
		'shame (infront of people)' : 'shame',
		'shame (infront of yourself)' : 'shame',
		'want happiness' : 'want happiness',
		'want love' : 'want love',
		'want respect' : 'want respect',
		'want salvation' : 'want salvation'
	}

	for i in range(len(row)):
		plabel = row[i]
		plabelInst = PostLabel.objects.get(title=plabel)
		for j in range(len(column)):
			emotion = column[j]
			emotionInst = Emotion.objects.get(title=emotion)
			slabel = emotion2slabel[emotion]
			slabelInst = SentenceLabel.objects.get(title=slabel)
			score = topic2emotion[i][j]
			if score > 0:
				q = EmotionTopicMap(plabel=plabelInst, slabel=slabelInst, emotion=emotionInst, score=score)
				q.save()

	data = {'message': "%s items added" % len(EmotionTopicMap.objects.all())}
	return HttpResponse(json.dumps(data), content_type='application/json')


def initial_VerseMap(request):
	print('initial_VerseMap')

	# VerseTopicMap
	f = open('knowledge_base/static/knowledge_base/initial_data/VerseTopicMap.txt','r') 

	items = VerseTopicMap.objects.all()
	for item in items: item.delete()
	print('all VerseTopicMap deleted')

	for line in f:
		line = line.replace('\n','') 
		line = line.strip()
		[plabel, verse, introduction, score] = line.split('$$$')
		plabelInst = PostLabel.objects.get(title=plabel)
		verseInst = Verse.objects.get(title=verse)
		commentInst = Comment.objects.get(introduction=introduction)
		q = VerseTopicMap(plabel=plabelInst, verse=verseInst, comment=commentInst, score=score)
		q.save()

	f.close()

	# VerseTopicMap
	f = open('knowledge_base/static/knowledge_base/initial_data/VerseEmotionMap.txt','r') 

	items = VerseEmotionMap.objects.all()
	for item in items: item.delete()
	print('all VerseEmotionMap deleted')

	for line in f:
		line = line.replace('\n','') 
		line = line.strip()
		[emotion, verse, introduction, score] = line.split('$$$')
		emotionInst = Emotion.objects.get(title=emotion)
		verseInst = Verse.objects.get(title=verse)
		commentInst = Comment.objects.get(introduction=introduction)
		q = VerseEmotionMap(emotion=emotionInst, verse=verseInst, comment=commentInst, score=score)
		q.save()

	f.close()


	data = {'message': "%s items added (VerseTopicMap) %s items added (VerseEmotionMap)" % (len(VerseTopicMap.objects.all()), len(VerseEmotionMap.objects.all()))}
	return HttpResponse(json.dumps(data), content_type='application/json')

def sentence_clean(line):
	line = line.strip()
	line = re.sub(r'^\s*(.+)\s*$',r'\1', line)
	line = line.replace(';','') 
	line = re.sub(r'[\.\?\!\$\£\%\^\&\*\(\)\-\_\=\+\[\]\#\~\@\<\>\/\\]+','', line)
	if re.match(r'^\s*$',line): return None
	line = line.strip()
	return line

def sentence2pk(line):
	try:
		sentence = Sentence.objects.get(body=line)
		return sentence
	except:
		print('-- Not in the DB! --')
		print(line)
		print()
		return None











