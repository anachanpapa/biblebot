from django.contrib import admin
from .models import PostLabel, Post, PostMembership, SentenceLabel, SentenceLabelSynonym, Sentence, SentenceMembership, SentenceInPost, Verse, Emotion, EmotionTopicMap, Comment, VerseTopicMap, VerseEmotionMap, Question, Answer


admin.site.register(PostLabel)
admin.site.register(Post)
admin.site.register(PostMembership)
admin.site.register(SentenceLabel)
admin.site.register(SentenceLabelSynonym)
admin.site.register(Sentence)
admin.site.register(SentenceMembership)
admin.site.register(SentenceInPost)
admin.site.register(Verse)
admin.site.register(Emotion)
admin.site.register(EmotionTopicMap)
admin.site.register(Comment)
admin.site.register(VerseTopicMap)
admin.site.register(VerseEmotionMap)
admin.site.register(Question)
admin.site.register(Answer)
