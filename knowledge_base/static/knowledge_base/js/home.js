$(document).ready(function() {



    $('.start').click(function(){
        if($("#vent-box").hasClass('with-sample')) {
            $("#vent-box").val('');
            $("#vent-box").focus();
            $("#vent-box").css('color','#333');
            $("#vent-box").removeClass('with-sample');
            $('.start').css('display','none');
        }
    });

    $('#vent-box').focusin(function(){
        if($(this).hasClass('with-sample')) {
            $(this).val('');
            $(this).css('color','#333');
            $(this).removeClass('with-sample');
            $('.start').css('display','none');
        }
    });    

    $('#vent-box').focusout(function(){
        sample = "(example)\nI am a gambling addict. I spent all my savings and in debt right now.\nI feel disappointed in myself but just can't control myself.\nSo scared of the thought that this will destroy my life.";

        var vent_content = $(this).val();
        if (vent_content.match(/^\s*$/)){
            $(this).addClass('with-sample');
            $(this).val(sample);
            $(this).css('color','#999');
            $('.start').css('display','block');
        }    
    });   

    // AJAX POST
    $('.vent-it').click(function(){
        // var $this = $(this);
        // $this.button('loading');
        var l = Ladda.create(this);
        l.start();
        var vent_content = $("#vent-box").val();
        // alert($("#vent-box").html());
        if (vent_content.match(/<.+>/)){
            alert('Your input contains HTML. Please try again');
            l.stop();
            return;
        }        

        vent_content = vent_content.replace('(example)\n','');
        $.ajax({
            type: "POST",
            url: "/knowledge_base/vent/",
            dataType: "json",
            data: { "item": vent_content },
            success: function(data) {
                // $this.button('reset');
                // alert(JSON.stringify(data[0]));
                var reply_html = makeReplyHTML(data);
                console.log(reply_html);
                $("#reply-box").html(reply_html);
            }
        }).always(function() { l.stop(); });;
        return false;
    });

    // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    }); 


});

function makeReplyHTML(reply_ordered){
    var reply_html = '<div id="accordion" role="tablist" aria-multiselectable="true">';
    for (i = 0; i < Object.keys(reply_ordered).length; i++) {
        content = reply_ordered[i];
        introduction = content['introduction'];
        verse_body = content['verse_body'];
        verse_url = content['verse_url'];
        postscript = content['postscript'];
        ps_html = '';
        if (postscript.length > 0) {
            ps_html = `<div class="comment-block"><div class="bbsays">BibleBot says:</div>${postscript}</div>`;
        }

        var card =
        `<div class="card">\
            <div class="card-header" role="tab" id="heading${i}">\
              <span class="induction">\
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse${i}" aria-expanded="true" aria-controls="collapse${i}">\
                  <span class="glyphicon glyphicon-book">&nbsp;</span>${introduction}\
                </a>\
              </span>\
            </div>\
            <div id="collapse${i}" class="collapse" role="tabpanel" aria-labelledby="heading${i}">\
              <div class="card-block"><div class="book-verse">${verse_url}</div>\
              ${verse_body}</div>${ps_html}\
            </div>\
        </div>`;

        reply_html = reply_html + card;
    }    
    reply_html = reply_html + '</div>';
    return reply_html;
}