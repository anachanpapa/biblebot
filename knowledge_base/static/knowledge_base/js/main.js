$(document).ready(function() {

    // AJAX POST

    $('.svm_learn_posts').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/svm_learn_posts/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                // alert(data.message);
                location.reload();
            }
        });

    });

    $('.svm_learn_sentences').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/svm_learn_sentences/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                // alert(data.message);
                location.reload();
            }
        });

    });


    $('#initial_PostLabel').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_PostLabel/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_SentenceLabel').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_SentenceLabel/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_SentenceLabelSynonym').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_SentenceLabelSynonym/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_Sentence').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_Sentence/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_Post').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_Post/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_Verse').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_Verse/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_Emotion').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_Emotion/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_EmotionTopicMap').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_EmotionTopicMap/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_Comment').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_Comment/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#initial_VerseMap').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/initial_VerseMap/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    $('#posts_into_catgories').click(function(){
        $.ajax({
            type: "POST",
            url: "/knowledge_base/posts_into_catgories/",
            dataType: "json",
            data: { "item": 'data_from_browser' },
            success: function(data) {
                alert(data.message);
            }
        });

    });

    // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    }); 


});