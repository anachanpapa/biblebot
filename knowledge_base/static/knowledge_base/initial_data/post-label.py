import json
import re

# f = open('therapy-clean.jl', 'r')
f = open('psychforums.jl', 'r')

for line in f:
	line = line.replace('\n','')
	line = line.replace('\r','')
	line = line.replace('\\','')
	line = line.replace('^','')
	line = line.replace('\,','')
	line = line.replace('"title": null,','"title": "null",')
	line = line.replace('"body": null,','"body": "null",')
	line = line.replace('"','\\"')
	line = line.replace('\\"title\\": \\"','"title": "')
	line = line.replace('\\", \\"body\\": \\"','", "body": "')
	line = line.replace('\\", \\"url\\": \\"', '", "url": "')
	line = line.replace('\\"}', '"}')
	line = line.replace('\\"', '')
	line = re.sub(r'[\$\£\%\^\&\*\_\=\+\#\~\@]+','', line)
	line = re.sub(r'[0-9]+','', line)
	line = line.lower()


	l = json.loads(line)
	url =l['url']
	# print(url)
	m = re.match(r"http://blahtherapy.com/user-groups/([^\/]+)/forum/", url)
	if m == None: m =re.match(r"http://www.psychforums.com/([^\/]+)/topic", url)
	if m :	print( m.group(1))
f.close()