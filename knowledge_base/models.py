from django.db import models
from datetime import datetime

# Create your models here.


# yet to add 
#abortion
#physical_disease
#financial
class PostLabel(models.Model):
	title = models.CharField(max_length=100)
	positives = models.IntegerField(default=0)
	precision = models.FloatField(default=-1)
	recall = models.FloatField(default=-1)

	def __str__(self): return u'{}'.format(self.title)
	class Meta:
		ordering = ["title"]		

class Post(models.Model):
	title = models.CharField(max_length=300)
	body = models.TextField()
	url = models.CharField(max_length=400)
	def __str__(self): return u'{}'.format(self.title)

class PostMembership(models.Model):	
	label = models.ForeignKey(PostLabel, on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	score = models.FloatField(default=-1)
	date_created = models.DateTimeField(default=datetime.now)
	class Meta:
		ordering = ["label"]			
	def __str__(self): return u"{0} -- {1}".format(self.label, self.post.title)

class SentenceLabel(models.Model):
	title = models.CharField(max_length=100)
	positives = models.IntegerField(default=0)
	supervisors = models.IntegerField(default=0)
	precision = models.FloatField(default=-1)
	recall = models.FloatField(default=-1)
	class Meta:
		ordering = ["title"]	
	def __str__(self): return u'{}'.format(self.title)


class SentenceLabelSynonym(models.Model):
	label = models.ForeignKey(SentenceLabel, on_delete=models.CASCADE)
	synonym = models.CharField(max_length=100)
	class Meta:
		ordering = ["label", "synonym"]	
	def __str__(self): return u'{0} -- {1}'.format(self.label.title, self.synonym)


class Sentence(models.Model):
	sentenceNo = models.IntegerField(default=-1)
	body = models.TextField()
	def __str__(self): return u'{}'.format(self.body)


class SentenceMembership(models.Model):
	label = models.ForeignKey(SentenceLabel, on_delete=models.CASCADE)
	sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
	score = models.FloatField(default=-1)
	date_created = models.DateTimeField(default=datetime.now)
	supervise = models.BooleanField(default=False)
	class Meta:
		ordering = ["label"]		
	def __str__(self): return u"{0} -- {1}".format(self.label, self.sentence.body)


class SentenceInPost(models.Model):
	sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	def __str__(self): return u"{0} -- {1}".format(self.sentence, self.post.title)


class Verse(models.Model):
	title = models.CharField(max_length=30)
	body = models.TextField()
	url = models.CharField(max_length=60)
	class Meta:
		ordering = ["title"]			
	def __str__(self): return u'{}'.format(self.title)


class Emotion(models.Model):
	title = models.CharField(max_length=40)
	def __str__(self): return u'{}'.format(self.title)


class EmotionTopicMap(models.Model):
	plabel = models.ForeignKey(PostLabel, on_delete=models.CASCADE)
	slabel = models.ForeignKey(SentenceLabel, on_delete=models.CASCADE)
	emotion = models.ForeignKey(Emotion, on_delete=models.CASCADE)
	score = models.FloatField(default=-1)
	class Meta:
		ordering = ["plabel", "slabel"]		
	def __str__(self): return u'{0} -- {1} -- {2} -- {3}'.format(self.plabel.title, self.slabel.title, self.emotion.title, self.score)


class Comment(models.Model):
	introduction = models.CharField(max_length=200)
	postscript = models.CharField(max_length=500, default="")
	class Meta:
		ordering = ["introduction"]	
	def __str__(self): return u'{}'.format(self.introduction)


class VerseTopicMap(models.Model):
	plabel = models.ForeignKey(PostLabel, on_delete=models.CASCADE)
	verse = models.ForeignKey(Verse, on_delete=models.CASCADE)
	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	score = models.FloatField(default=-1)
	class Meta:
		ordering = ["plabel", "score"]
	def __str__(self): return u'{0} -- {1} -- {2} -- {3}'.format(self.plabel.title, self.score, self.verse.title, self.comment.introduction)


class VerseEmotionMap(models.Model):
	emotion = models.ForeignKey(Emotion, on_delete=models.CASCADE)
	verse = models.ForeignKey(Verse, on_delete=models.CASCADE)
	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	score = models.FloatField(default=-1)
	class Meta:
		ordering = ["emotion", "score"]
	def __str__(self): return u'{0} -- {1} -- {2} -- {3}'.format(self.emotion.title, self.score, self.verse.title, self.comment.introduction)


class Question(models.Model):
	body = models.TextField()	
	def __str__(self): return u'{}'.format(self.body)


class Answer(models.Model):
	title = models.CharField(max_length=20)
	body = models.TextField()	
	def __str__(self): return u'{}'.format(self.title)

# class AnswerRule(models.Model):
	
